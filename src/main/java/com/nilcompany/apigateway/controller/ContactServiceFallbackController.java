package com.nilcompany.apigateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactServiceFallbackController {

	@GetMapping("/contactservicefallback")
	public String handleFallback() {
		return "Contact service is down at this moment !!";
	}
}
