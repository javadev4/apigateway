package com.nilcompany.apigateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserServiceFallbackController {

	@GetMapping("/userservicefallback")
	public String handleFallbackURL() {
		return "User Service is down at this moment !!";
	}
}
